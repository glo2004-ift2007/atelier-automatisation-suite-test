## Atelier d'automatisation d'une suite de tests

Durant cet atelier, vous serez introduits à deux nouveaux plugins Maven : Maven Surefire et Jacoco. Ils permettent respectivement lors de la compilation de votre code d’exécuter automatiquement des tests unitaires et de générer la couverture de code de votre application.

À partir de la base de code fournie, qui est la même que la base de code de solution de l’atelier sur l’introduction à _Maven_, vous apprendrez comment configurer votre projet et le _pom.xml_ de façon à pouvoir utiliser ces deux plugins.  Ces deux plugins vous permettront d’utiliser des tests unitaires dans votre projet, un aspect essentiel à tout projet.

1. Ajoutez le plugin _maven-surefire-plugin_ présenté ci-dessous à votre fichier _pom.xml_ en suivant la méthode présentée lors de _l'Atelier d'introduction à l'outil de gestion des dépendances _Maven_. Ce plugin est essentiel afin d'exécuter les tests unitaires et permet aussi de générer un rapport comprennant les échecs et les réussites.

    ```
    <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.22.1</version>
    </plugin>
    ```

2. Ajoutez aussi la dépendance à _JUnit_ présentée ci-dessous en prenant soin de suivre encore une fois la méthode présentée lors de l'atelier précédent. _JUnit_ est un _framework_ qui permet d'écrire et d'exécuter des tests unitaires.

    ```
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.11</version>
        <scope>test</scope>
    </dependency>
    ```

3. Créez la structure de fichier suivante dans le répertoire _src_, car le _package_ testé est celui du domaine.

    ```
    src/test/java/ca/ulaval/glo2004/domain/basket
    ```

4. Créez ensuite la classe _BasketItemTest.java_, celle-ci contiendra l'ensemble des tests à exécuter. Le premier test bidon ci-dessous permet de vérifier si votre environnement de test est fonctionnel. 


    ```
    package ca.ulaval.glo2004.domain.basket;

    import org.junit.Test;
    import java.awt.*;
    import static org.junit.Assert.*;

    public class BasketItemTest {
        /**
        * Exemple de test avec JUnit
        */
        @Test
        public void premierTestBidon()
        {
            assertTrue(true);
        }
    }
    ```

5. Cliquez droit sur le projet dans la colonne de gauche, sélectionnez ensuite _Run Maven -> Goals_ et dans ce champ entrez la commande _Maven_ ci-dessous avant de l'exécuter. Le résultat des tests sera affiché à l'écran, normalement le taux de passage devrait être de 100% puisque le test est bidon (on vérifie si _True_ est bel et bien _True_).

    ```
    clean install test
    ```

6. Ajoutez ensuite le plugin _Jacoco_ à votre fichier _pom.xml_, en suivant la même méthode qu'utilisée précédemment. Ce plugin permet de générer la couverture de code de votre application. Lorsque vous utilisez la commande pour exécuter les tests unitaires, la couverture de code de votre application sera automatiquement analysée rapport _html_ sera généré vous permettant de voir chaque méthode de chaque classe de votre domaine ainsi que la couverture par vos tests unitaires. Cela permet donc d’identifier facilement ce qu’il reste à tester dans le domaine.

    ```
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.10</version>
    </plugin>
    ```

7. Roulez ensuite la commande ci-dessous de la même façon qu'à l'étape 5. 

    ```
    clean install jacoco:report
    ```

    Vous devriez avoir l'erreur _Skipping JaCoCo execution due to missing execution data file_, ceci est dû au fait qu'un agent doit être configuré afin d'obtenir les résultats des tests effectués à l'aide de _JUnit_.

8. Ajoutez l'agent afin d'obtenir le plugin complet ci-dessous.

    ```
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.10</version>

        <executions>
            <execution>
                <id>jacoco-initialize</id>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
            </execution>
            <execution>
                <id>jacoco-site</id>
                <phase>package</phase>
                <goals>
                    <goal>report</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    ```

    Roulez de nouveau la commande présentée à l'étape 7, allez ensuite dans le répertoire _target\site\jacoco_, le fichier _index.html_ disponible dans ce répertoire est le rapport généré par _Jacoco_, ouvrez-le avec le navigateur de votre choix (comme _Google Chrome_ par exemple), celui-ci devrait montrer une couverture du code nulle.

9. Ajoutez des tests unitaires dans la classe _BasketItemTest.java_ de façon à avoir une couverture complète (100 %) de la classe _BasketItem.java_. Deux métriques sont utilisés dans _Jacoco_ soit _Missed Instructions_ et _Missed Branches_, des explications détaillées de ces métriques et du contenu du rapport généré sont disponibles en suivant ce [lien](https://www.baeldung.com/jacoco). Veuillez prendre en note que pour la réalisation de cet atelier, il est prit en compte que les bonnes pratiques ainsi que le fonctionnement des tests unitaires sont des connaissances déjà acquises par l'étudiant, veuillez vous référer à l'atelier réalisé à cet effet. Un exemple est disponible au besoin au chapitre 2 de ce [lien](https://www.vogella.com/tutorials/JUnit/article.html).

    Voici quelques points importants à appliquer lors de la création de vos tests :
* Règle générale, assurez vous de respecter le concept _FIRST_, Fast, Independant, Repeatable, Self-validating et Thorough. C'est donc dire que vos tests doivent être rapides à exécuter, ne doivent pas dépendre les uns des autres, sont répétables, échouent ou passent en retournant un booléen et couvrent les différents cas de manière exhaustive. 
* Pour le nommage des tests, utilisez le principe _given-when-then_. Ainsi pour tester la méthode _contains_ afin de voir si elle retourne bel et bien _True_ lorsqu'un point donné est à l'intérieur du cercle, un test à réaliser serait *givenPointInsideCircle_whenContains_thenReturnTrue*
* Afin de mieux structurer les tests unitaires de manière claire et compréhensible, organisez les selon la convention _arrange-act-assert_.

10. Une fois l'atelier complété, vous pouvez comparer ce que vous avez obtenu avec le corrigé disponible dans ce même répertoire sous la branche _solution_.
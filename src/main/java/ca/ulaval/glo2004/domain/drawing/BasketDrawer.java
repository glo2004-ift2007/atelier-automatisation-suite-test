package ca.ulaval.glo2004.domain.drawing;


import ca.ulaval.glo2004.domain.basket.BasketController;
import ca.ulaval.glo2004.domain.basket.BasketItem;
import ca.ulaval.glo2004.domain.basket.BasketItemReadOnly;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.List;


public class BasketDrawer {
	
    private final BasketController controller;
    private Dimension initialDimension;


    public BasketDrawer(BasketController controller, Dimension initialDimension) {
            this.controller = controller;
            this.initialDimension = initialDimension;
    }

    public void draw(Graphics g) {
            drawBasket(g);
            drawFruits(g);
    }

    private void drawFruits(Graphics g) {
            List<BasketItemReadOnly> items = controller.getBasketItemList();
            for (BasketItemReadOnly item: items){
                    Point fruitPoint = item.getPoint();
                    int radius = item.getRadius();
                    if (item.isSelected()) {
                            g.setColor(new Color(255, 0, 0));
                            int offsetRadius = radius + 1;
                            g.fillOval((int) fruitPoint.getX() - offsetRadius,(int) fruitPoint.getY() - offsetRadius, offsetRadius * 2, offsetRadius * 2);
                    }
                    Color color = item.getColor();
                    g.setColor(color);
                    g.fillOval((int)fruitPoint.getX()-radius,(int)fruitPoint.getY()-radius, radius*2, radius*2);
            }
    }

    private void drawBasket(Graphics g) {
            int width = (int) initialDimension.getWidth();
            int height = (int) initialDimension.getHeight();
    g.setColor(new Color(140,98,57));
    g.fillRect(width/4, (int)(height/1.75), width/2, height/4);
    }
}

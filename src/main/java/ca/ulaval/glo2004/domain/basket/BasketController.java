package ca.ulaval.glo2004.domain.basket;

import java.awt.Color;
import java.awt.Point;
import java.util.List;
import java.util.stream.Collectors;

public class BasketController {

    private final Basket basket;

    public BasketController() {
        basket = new Basket();
    }

    public void addApple(Point mousePoint) {
        BasketItem newApple = new BasketItem(mousePoint, Color.RED);
        basket.add(newApple);
    }

    public void addOrange(Point mousePoint) {
        BasketItem newOrange = new BasketItem(mousePoint, new Color(245, 158, 66));
        basket.add(newOrange);
    }

    public List<BasketItemReadOnly> getBasketItemList() {
        return basket.getFruitList()
                .stream()
                .map(basketItem -> (BasketItemReadOnly) basketItem)
                .collect(Collectors.toList());
    }

    public void switchSelectionStatus(double x, double y) {
        basket.switchSelectionStatus(x, y);
    }

    public void updateSelectedItemsPositions(Point delta) {
        basket.updateSelectedItemsPosition(delta);
    }
}

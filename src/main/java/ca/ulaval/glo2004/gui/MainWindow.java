/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.ulaval.glo2004.gui;
import ca.ulaval.glo2004.domain.basket.BasketController;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import javax.swing.SwingUtilities;

public class MainWindow extends javax.swing.JFrame
{
    public BasketController controller;
    
    private ApplicationMode actualMode;
    
    // Ces attributs servent à la gestion du déplacement.
    public Point actualMousePoint = new Point();
    public Point delta = new Point();

    
    public enum ApplicationMode {
        SELECT,ADD
    }
    
    public MainWindow()
    {
        controller = new BasketController();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createFruitButtonGroup = new javax.swing.ButtonGroup();
        mainPanel = new javax.swing.JPanel();
        buttonTopPanel = new javax.swing.JPanel(new FlowLayout(FlowLayout.LEFT));
        selectionButton = new javax.swing.JToggleButton();
        additionButton = new javax.swing.JToggleButton();
        itemTypeBox = new javax.swing.JComboBox();
        jSplitPane1 = new javax.swing.JSplitPane();
        mainScrollPane = new javax.swing.JScrollPane();
        drawingPanel = new ca.ulaval.glo2004.gui.DrawingPanel(this);
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        topMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openMenuItem = new javax.swing.JMenuItem();
        quitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();

        createFruitButtonGroup.add(selectionButton);
        createFruitButtonGroup.add(additionButton);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mes Ateliers");

        mainPanel.setLayout(new java.awt.BorderLayout());

        buttonTopPanel.setPreferredSize(new java.awt.Dimension(400, 35));

        selectionButton.setSelected(true);
        selectionButton.setText("Mode Sélection");
        selectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectionButtonActionPerformed(evt);
            }
        });
        buttonTopPanel.add(selectionButton);

        additionButton.setText("Mode Ajout");
        additionButton.setToolTipText("");
        additionButton.setPreferredSize(new java.awt.Dimension(105, 23));
        additionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                additionButtonActionPerformed(evt);
            }
        });
        buttonTopPanel.add(additionButton);

        itemTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "APPLE", "ORANGE" }));
        itemTypeBox.setPreferredSize(new java.awt.Dimension(105, 23));
        buttonTopPanel.add(itemTypeBox);

        mainPanel.add(buttonTopPanel, java.awt.BorderLayout.NORTH);

        jSplitPane1.setMinimumSize(new java.awt.Dimension(0, 202));
        jSplitPane1.setPreferredSize(new Dimension(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width, (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().height*0.5)));

        mainScrollPane.setMinimumSize(new java.awt.Dimension(0, 0));
        mainScrollPane.setPreferredSize(new Dimension((int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width*0.85), (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().height*0.5)));

        drawingPanel.setPreferredSize(new java.awt.Dimension(0, 0));
        drawingPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                drawingPanelMousePressed(evt);
            }
        });
        drawingPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                drawingPanelMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout drawingPanelLayout = new javax.swing.GroupLayout(drawingPanel);
        drawingPanel.setLayout(drawingPanelLayout);
        drawingPanelLayout.setHorizontalGroup(
            drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1598, Short.MAX_VALUE)
        );
        drawingPanelLayout.setVerticalGroup(
            drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 538, Short.MAX_VALUE)
        );

        mainScrollPane.setViewportView(drawingPanel);

        jSplitPane1.setLeftComponent(mainScrollPane);

        jTabbedPane1.setPreferredSize(new java.awt.Dimension(0, 540));

        jPanel1.setPreferredSize(new Dimension((int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().width*0.15), (int)(java.awt.Toolkit.getDefaultToolkit().getScreenSize().height*0.75)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 975, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 512, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Facturation", null, jPanel1, "");

        jSplitPane1.setRightComponent(jTabbedPane1);

        mainPanel.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        openMenuItem.setText("Open");
        fileMenu.add(openMenuItem);

        quitMenuItem.setText("Quit");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitMenuItem);

        topMenuBar.add(fileMenu);

        editMenu.setText("Edit");
        topMenuBar.add(editMenu);

        setJMenuBar(topMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1877, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 577, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectionButtonActionPerformed
        this.setMode(ApplicationMode.SELECT);
    }//GEN-LAST:event_selectionButtonActionPerformed

    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitMenuItemActionPerformed

    private void additionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_additionButtonActionPerformed
        this.setMode(ApplicationMode.ADD);
    }//GEN-LAST:event_additionButtonActionPerformed

    private void drawingPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMousePressed
        Point mousePoint = evt.getPoint();
        this.actualMousePoint = mousePoint;
        if (this.actualMode == ApplicationMode.ADD){
            String itemType = (String) this.itemTypeBox.getSelectedItem();
            if (itemType.equals("APPLE")){
                this.controller.addApple(mousePoint);
            }
            else {
                this.controller.addOrange(mousePoint);
            }
            drawingPanel.repaint();
        }
        else if (this.actualMode == ApplicationMode.SELECT && SwingUtilities.isLeftMouseButton(evt)){
            // Très bel endroit pour traduire les coordonnées d'affichage en coordonnées réelles.. Je dis cela comme ça, par pur hasard ;)
            this.controller.switchSelectionStatus(mousePoint.getX(), mousePoint.getY());
            drawingPanel.repaint();
        }
    }//GEN-LAST:event_drawingPanelMousePressed

    private void drawingPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_drawingPanelMouseDragged
        if (SwingUtilities.isRightMouseButton(evt)) {
                delta.setLocation((evt.getX() - this.actualMousePoint.x),(evt.getY() - this.actualMousePoint.y));
                this.controller.updateSelectedItemsPositions(delta);
                this.actualMousePoint = evt.getPoint();
                drawingPanel.repaint();
        }
    }//GEN-LAST:event_drawingPanelMouseDragged

    public void setMode(ApplicationMode newMode) {
        this.actualMode = newMode;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton additionButton;
    private javax.swing.JPanel buttonTopPanel;
    private javax.swing.ButtonGroup createFruitButtonGroup;
    private ca.ulaval.glo2004.gui.DrawingPanel drawingPanel;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JComboBox itemTypeBox;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JScrollPane mainScrollPane;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem quitMenuItem;
    private javax.swing.JToggleButton selectionButton;
    private javax.swing.JMenuBar topMenuBar;
    // End of variables declaration//GEN-END:variables
}

